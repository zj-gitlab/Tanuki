defmodule Tanuki.GroupsTest do
  use ExUnit.Case, async: false
  use ExVCR.Mock, adapter: ExVCR.Adapter.Hackney

  @client Tanuki.Client.new(%{private_token: "A28e6ZeJHNJQLN6r6yTv"}, "http://localhost:3000/api/v3/")
  alias Tanuki.Groups

  setup_all do
    ExVCR.Config.cassette_library_dir("test/fixtures/vcr_cassettes")
    HTTPoison.start
  end

  test "Groups.list/2" do
    use_cassette "groups_list" do
      assert Enum.count(Groups.list(@client)) == 4
    end
  end

  test "Groups.create/2" do
    group = %{
      name: "Tanuki",
      path: "tanuki",
      description: "Mepmep"
    }
    use_cassette "groups_create" do
      assert Groups.create(@client, group)
    end
  end

  test "Groups.transfer/2" do
    use_cassette "groups_transfer" do
      assert Groups.transfer(1, 1, @client)
    end
  end

  test "Groups.projects/2" do
    use_cassette "groups_projects" do
      assert (Groups.projects(2, @client) |> Enum.count) == 1
    end
  end

  test "Groups.delete/2" do
    use_cassette "groups_delete" do
      assert Groups.delete(4, @client)
    end
  end
end
