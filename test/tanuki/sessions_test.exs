defmodule Tanuki.SesionsTest do
  use ExUnit.Case, async: false
  use ExVCR.Mock, adapter: ExVCR.Adapter.Hackney

  alias Tanuki.Sessions
  @client Tanuki.Client.new(%{private_token: nil}, "http://localhost:3000/api/v3/")

  setup_all do
    ExVCR.Config.cassette_library_dir("test/fixtures/vcr_cassettes")
    HTTPoison.start
  end

  test "Sessions.create/1" do
    login = %{
      username: "root",
      password: "5iveL!fe"
    }
    use_cassette "sessions_create" do
      assert Sessions.create(@client, login)
    end
  end
end
