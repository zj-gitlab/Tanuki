defmodule Tanuki.Project.CommitsTest do
  use ExUnit.Case, async: false
  use ExVCR.Mock, adapter: ExVCR.Adapter.Hackney

  @client Tanuki.Client.new(%{private_token: "g8zWsDJqs5LCn9-JRmJh"}, "http://localhost:8080/api/v3/")
  @sha "dd19d54709dbe4abe1641623722bfe80d8564748"
  alias Tanuki.Projects.Repository.Commits

  setup_all do
    ExVCR.Config.cassette_library_dir("test/fixtures/vcr_cassettes")
    HTTPoison.start
  end

  test "Commits.list/2" do
    use_cassette "project_commits_list", do: assert (Commits.list(1, @client) |> Enum.count) == 20
  end

  test "Commits.find/3" do
    use_cassette "project_commits_find" do
      assert Commits.find(1, @sha, @client).author_email == "git@michael.ficarra.me"
    end
  end

  test "Commits.diff/3" do
    use_cassette "project_commits_diff" do
      assert Commits.diff(1, @sha, @client)
    end
  end

  test "Commits.comments/3" do
    use_cassette "project_commits_comments", do: assert Commits.comments(1, @sha, @client)
  end
end
