defmodule Tanuki.Projects.Members do
  @doc """
  GET /projects/:id/members

  Get a list of a project's team members. WHere project_id is passed as first argument.

  Parameters:
    query (optional) - Query string to search for members
  """
  def list(id, client, params \\ []), do: Tanuki.get("projects/#{id}/members", client, params)

  @doc """
  GET /projects/:id/members/:user_id

  Gets a project team member.
  """
  def find(id, user_id, client), do: Tanuki.get("projects/#{id}/members/#{user_id}", client)

  @doc """
  POST /projects/:id/members

  Adds a user to a project team. This is an idempotent method and can be called multiple times with the same parameters. Adding team membership to a user that is already a member does not affect the existing membership.

  Parameters:
  - user_id (required) - The ID of a user to add
  - access_level (required) - Project access level
  """
  def create(id, client, params), do: Tanuki.post("projects/#{id}/members", client, params)

  @doc """
  PUT /projects/:id/members/:user_id

  Updates a project team member to a specified access level.

  Parameters:
  - access_level (required) - Project access level
  """
  def modify(id, uid, client, params), do: Tanuki.put("projects/#{id}/members/#{uid}", client, params)

  @doc """
  DELETE /projects/:id/members/:user_id

  Removes a user from a project team.
  """
  def delete(id, uid, client), do: Tanuki.delete("projects/#{id}/members/#{uid}", client)
end
