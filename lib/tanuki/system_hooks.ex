defmodule Tanuki.SystemHooks do
  @doc """
  GET /hooks

  Get list of system hooks
  """
  def list(client), do: Tanuki.get("hooks", client)

  @doc """
  POST /hooks

  Add new system hook

  Parameters:
  - url (required) - The hook url
  """
  def create(client, params), do: Tanuki.post("hooks", client, params)

  @doc """
  GET /hooks/:id

  Test System Hook
  """
  def test(id, client), do: Tanuki.get("hooks/#{id}", client)

  @doc """
  DELETE /hooks/:id

  Deletes a system hook. This is an idempotent API function and returns 200 OK even if the hook is not available. If the hook is deleted it is also returned as JSON.
  """
  def delete(id, client), do: Tanuki.delete("hooks/#{id}", client)
end
