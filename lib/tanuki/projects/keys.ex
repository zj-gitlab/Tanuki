defmodule Tanuki.Projects.Keys do
  @moduledoc """
  Module to interactive with Deploy Keys for a given project(by ID)
  """

  @doc """
  GET /projects/:id/keys

  Get a list of a project's deploy keys.
  """
  def list(id, client), do: Tanuki.get("projects/#{id}/keys", client)

  @doc """
  GET /projects/:id/hooks/:hook_id

  Get a specific hook for a project.
  """
  def find(id, key_id, client), do: Tanuki.get("projects/#{id}/keys/#{key_id}", client)

  @doc """
  POST /projects/:id/keys

  Creates a new deploy key for a project. If deploy key already exists in another project - it will be joined to project but only if original one was is accessible by same user

  Parameters:
  - title (required) - New deploy key's title
  - key (required) - New deploy key

  """
  def create(id, client, params), do: Tanuki.post("projects/#{id}/keys", client, params)

  @doc """
  DELETE /projects/:id/keys/:key_id

  Delete a deploy key from a project
  """
  def delete(id, key_id, client), do: Tanuki.delete("projects/#{id}/keys/#{key_id}", client)
end
