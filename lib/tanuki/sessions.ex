defmodule Tanuki.Sessions do
  @doc """
  POST /session

  Login to get private token

  Parameters:
  - login (required) - The login of user
  - email (required if login missing) - The email of user
  - password (required) - Valid password

  NB. No token is needed here, the client can be initiated with "" as token
  """
  def create(client, params), do: Tanuki.post("session", client, params)
end
