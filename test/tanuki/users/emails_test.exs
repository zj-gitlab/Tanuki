defmodule Tanuki.UsersEmailsTest do
  use ExUnit.Case, async: false
  use ExVCR.Mock, adapter: ExVCR.Adapter.Hackney

  @client Tanuki.Client.new(%{private_token: "g8zWsDJqs5LCn9-JRmJh"}, "http://localhost:3000/api/v3/") # Dev kit, so the token is useless. ;)
  alias Tanuki.Users.Emails

  setup_all do
    ExVCR.Config.cassette_library_dir("test/fixtures/vcr_cassettes")
    HTTPoison.start
  end

  test "Emails.mine/1" do
    use_cassette "emails_mine" do
      assert Emails.mine(@client)
    end
  end

  test "Emails.find/2" do
    use_cassette "emails_find" do
      assert Emails.find(3, @client)
    end
  end

  test "Emails.list/2" do
    use_cassette "emails_find_for_user" do
      assert Emails.list(5, @client) == []
    end
  end

  test "Emails.create/2" do
    email = %{ email: "Tanuki@mepmep.co" }
    use_cassette "emails_create" do
      assert Emails.create(@client, email)
    end
  end

  test "Emails.delete/2" do
    use_cassette "emails_delete" do
      refute Emails.delete(1, @client)
    end
  end
end
