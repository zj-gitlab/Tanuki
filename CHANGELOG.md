0.2.1
- Add endpoints for Todos
- Add support for Elixir 1.3
- Drop support for Elixir 1.2 and earlier
- Update Poison to 2.1

0.2
- Add a _lot_ of endpoints

0.1
- OAuth2 support
- Improve consistency API
- Add lots of tests
- All keys in the returned structs are atoms :)
